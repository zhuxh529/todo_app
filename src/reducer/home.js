import {CREATE_TASK_FROM_STORAGE,CREATE_TASK,EDIT_TASK,DELETE_TASK} from '../actions/type'
import AsyncStorage from '@react-native-async-storage/async-storage';

const _ = require('micro-dash');

const initialState = {
    tasks:[]
}

export default function(state=initialState, action) {
    switch(action.type) {
        case CREATE_TASK_FROM_STORAGE:
            return {...state,tasks:action.tasks_lists}

        case CREATE_TASK:
            // check value not null, empty
            if (_.isEmpty(action.value)){
                return {...state}
            }
            let tasks_lists=[...state.tasks]
            // check repeated value
            if(tasks_lists.includes(action.value)){
                alert(`${action.value} already exists. Do nothing.`)
                return {...state}
            }
            tasks_lists.push(action.value)
            AsyncStorage.setItem('tasks', JSON.stringify(tasks_lists))
            return {...state,tasks:tasks_lists}

        case EDIT_TASK:
            // check value not null, empty or index out of bound
            if (_.isEmpty(action.value) || action.index >= state.tasks.length){
                return {...state}
            }
            let edit_tasks=[...state.tasks]
            // check repeated value
            if(edit_tasks.includes(action.value)){
                alert(`${action.value} already exists. Do nothing.`)
                return {...state}
            }
            edit_tasks[action.index]=action.value
            AsyncStorage.setItem('tasks', JSON.stringify(edit_tasks))
            return {...state,tasks:edit_tasks}

        case DELETE_TASK:
            let delete_tasks=[...state.tasks]
            delete_tasks.splice(action.index,1)
            AsyncStorage.setItem('tasks', JSON.stringify(delete_tasks))
            return {...state,tasks:delete_tasks}

        default:
            return state;
    }
}