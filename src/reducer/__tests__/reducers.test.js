import reducer from '../home'
import {CREATE_TASK_FROM_STORAGE,CREATE_TASK,EDIT_TASK,DELETE_TASK} from '../../actions/type'
import AsyncStorageMock from '@react-native-async-storage/async-storage/jest/async-storage-mock';

//  mock for async-storage
jest.mock("@react-native-async-storage/async-storage", () =>
    require("@react-native-async-storage/async-storage/jest/async-storage-mock"),
);

//  tests start
describe('reducer home tests - CREATE_TASK_FROM_STORAGE', () => {
    it('test CREATE_TASK_FROM_STORAGE success', async () => {
        const new_item = ["test1", "test2"];
        let new_state = reducer(undefined, {type: CREATE_TASK_FROM_STORAGE, tasks_lists:new_item})
        expect(new_state).toEqual({tasks: new_item});
    });
});


describe('reducer home tests - CREATE_TASK', () => {
    it('test CREATE_TASK success', async () => {
        const value = "test1";
        const init_list = {tasks: ["defautl item"]}
        let new_state = reducer(init_list, {type: CREATE_TASK, value})
        expect(new_state).toEqual({tasks: [...init_list.tasks, value]});
        
        let tasks_in_storage = await AsyncStorageMock.getItem("tasks");
        expect(JSON.parse(tasks_in_storage)).toEqual([...init_list.tasks, value]);
    });

    it('test CREATE_TASK with empty value', async () => {
        const value = "";
        const init_list = {tasks: ["defautl item"]}
        let new_state = reducer(init_list, {type: CREATE_TASK, value})
        expect(new_state).toEqual(init_list);
    });

    it('test CREATE_TASK with undefined value', async () => {
        const value = undefined;
        const init_list = {tasks: ["defautl item"]}
        let new_state = reducer(init_list, {type: CREATE_TASK, value})
        expect(new_state).toEqual(init_list);
    });

    it('test CREATE_TASK with null value', async () => {
        const value = null;
        const init_list = {tasks: ["defautl item"]}
        let new_state = reducer(init_list, {type: CREATE_TASK, value})
        expect(new_state).toEqual(init_list);
    });

    it('test CREATE_TASK with repeated value', async () => {
        window.alert = jest.fn();
        const value = "test1";
        const init_list = {tasks: [value]}
        let new_state = reducer(init_list, {type: CREATE_TASK, value})
        expect(new_state).toEqual(init_list);
        window.alert.mockClear();
    });
});


describe('reducer home tests - EDIT_TASK', () => {
    it('test EDIT_TASK success', async () => {
        const value = "new updated item";
        const init_list = {tasks: ["item1", "item2"]}
        const new_list = {tasks: ["item1", value]}
        let new_state = reducer(init_list, {type: EDIT_TASK, value, index: 1})
        expect(new_state).toEqual(new_list);
        let tasks_in_storage = await AsyncStorageMock.getItem("tasks");
        expect(JSON.parse(tasks_in_storage)).toEqual(new_list.tasks);
    });

    it('test EDIT_TASK with empty value', async () => {
        const value = "";
        const init_list = {tasks: ["item1", "item2"]}
        let new_state = reducer(init_list, {type: EDIT_TASK, value, index: 1})
        expect(new_state).toEqual(init_list);
    });

    it('test EDIT_TASK with undefined value', async () => {
        const value = undefined;
        const init_list = {tasks: ["item1", "item2"]}
        let new_state = reducer(init_list, {type: EDIT_TASK, value, index: 1})
        expect(new_state).toEqual(init_list);
    });

    it('test EDIT_TASK with null value', async () => {
        const value = null;
        const init_list = {tasks: ["item1", "item2"]}
        let new_state = reducer(init_list, {type: EDIT_TASK, value, index: 1})
        expect(new_state).toEqual(init_list);
    });

    it('test EDIT_TASK with index out of bound', async () => {
        const value = "new item";
        const init_list = {tasks: ["item1", "item2"]};
        const index = 100;
        let new_state = reducer(init_list, {type: EDIT_TASK, value, index})
        expect(new_state).toEqual(init_list);
    });

    it('test EDIT_TASK with repeated value', async () => {
        window.alert = jest.fn();
        const value = "item1";
        const init_list = {tasks: ["item1", "item2"]}
        let new_state = reducer(init_list, {type: EDIT_TASK, value, index: 1})
        expect(new_state).toEqual(init_list);
        window.alert.mockClear();
    });
});


describe('reducer home tests - DELETE_TASK', () => {
    it('test DELETE_TASK success', async () => {
        const delete_index = 1;
        const init_list = {tasks: ["item1", "item2", "item3"]}
        const new_list = {tasks: ["item1", "item3"]}
        let new_state = reducer(init_list, {type: DELETE_TASK,  index: delete_index})
        expect(new_state).toEqual(new_list);
        let tasks_in_storage = await AsyncStorageMock.getItem("tasks");
        expect(JSON.parse(tasks_in_storage)).toEqual(new_list.tasks);
    });

    it('test DELETE_TASK with index out of bound', async () => {
        const delete_index = 100;
        const init_list = {tasks: ["item1", "item2", "item3"]}
        let new_state = reducer(init_list, {type: DELETE_TASK,  index: delete_index})
        expect(new_state).toEqual(init_list);
    });

});
