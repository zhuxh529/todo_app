import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
    StyleSheet,
    Modal
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import * as LocalAuthentication from 'expo-local-authentication';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {create_item_from_storage,creation_task,edit_tasks,delete_tasks} from './actions/home'

const _ = require('micro-dash');
  
const Home = () => {
    const dispatch = useDispatch();
    // input chars
    const [task, setTask] = useState("");

    // Modal datas for updating todo item
    const [ModalVisible, setModalVisible] = useState(false);
    const [ModalIndex, setModalIndex] = useState(-1);
    const [EditTask, setEditTask] = useState("");

    // task lists
    const tasks = useSelector((state) => state.home.tasks);

    useEffect(() => {
        // init TODO item list from storage
        AsyncStorage.getItem('tasks')
            .then((value) => {
                if(!_.isNil(value)){
                    const data = JSON.parse(value);
                    dispatch(create_item_from_storage(data))
                }
            });
    }, [])
  
    // create todo item
    const handleAddTask = async () => {
        if (task!='') {
            const results = await LocalAuthentication.authenticateAsync();
            if(!_.get(results, ["success"], false)){
                alert("You can't do it without authentication")
                return;
            }
            dispatch(creation_task(task))
            setTask("");
        }else{
            alert('Data cannot be empty')
        }
    };

    // update todo item
    const handleEditTask = async (index) => {
        if(EditTask!=''){
            const results = await LocalAuthentication.authenticateAsync();
            if(!_.get(results, ["success"], false)){
                alert("You can't do it without authentication")
                return;
            }
            setModalVisible(false)
            dispatch(edit_tasks(index,EditTask))
        }else{
            alert('Data cannot be empty')
        }
    };

    // delete todo item
    const handleDeleteTask = async (index) => {
        const results = await LocalAuthentication.authenticateAsync();
        if(!_.get(results, ["success"], false)){
            alert("You can't do it without authentication")
            return;
        }
        dispatch(delete_tasks(index))
    };


    // modals for updating items
    const handleCloseModal = ()=>{
        setModalVisible(false)
    }

    const OpenEditModal = (index) => {
        setModalVisible(true)
        setModalIndex(index)
        setEditTask(tasks[index])
    }

    const renderItem = ({ item, index }) => (
        <View style={styles.task}>
            <Text
                numberOfLines={2}
                style={styles.itemList}>{item}</Text>
            <View style={styles.taskButtons}>
                <TouchableOpacity
                    onPress={() => OpenEditModal(index)}>
                    <Text
                        style={styles.EditOpenModalButton}>Edit</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => handleDeleteTask(index)}>
                    <Text
                        style={styles.deleteButton}>Delete</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
  
    return (
        <View style={styles.container}>
            <Text style={styles.title}>ToDo App</Text>
            <TextInput
                style={styles.input}
                placeholder="Enter item name"
                value={task}
                onChangeText={(text) => setTask(text)}
            />
            <TouchableOpacity
                style={styles.addButton}
                onPress={handleAddTask}>
                <Text style={styles.addButtonText}>
                    Add Item
                </Text>
            </TouchableOpacity>
            <View style={styles.todoListTitle}><Text style={{fontSize:16}}>Todo Lists</Text></View>
            {tasks.length==0 && <Text style={{color: '#777'}}>You don't have any todo items</Text>}
            <FlatList
                data={tasks}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
            <Modal 
                animationType="slide"
                transparent={false}
                visible={ModalVisible}
                onRequestClose={() => {
                    setModalVisible(false);
                  }}
                >
                <View
                    style={styles.taskModal}>
                        <TextInput
                            style={styles.Modalinput}
                            placeholder="Enter task name"
                            value={EditTask}
                            onChangeText={(text) => setEditTask(text)}
                        />
                        <View style={styles.taskButtons}>
                            <TouchableOpacity
                                onPress={() => handleEditTask(ModalIndex)}>
                                <Text
                                    style={styles.editButton}>Update</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => handleCloseModal()}>
                                <Text
                                    style={styles.closeButton}>Close</Text>
                            </TouchableOpacity>
                            
                        </View>
                </View>
            </Modal>
        </View>
    );
};
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 40,
        marginTop: 40,
        backgroundColor:'#fff'
    },
    title: {
        fontSize: 24,
        fontWeight: "bold",
        marginBottom: 20,
    },
    input: {
        borderWidth: 3,
        borderColor: "#ccc",
        padding: 10,
        marginBottom: 10,
        borderRadius: 10,
        fontSize: 18,
    },
    EditOpenModalButton: {
        marginRight: 10,
        color: "#3D1198",
        fontWeight: "bold",
        fontSize: 18,
    },
    todoListTitle: {
        padding: 10,
        marginBottom: 16,
        marginTop: 16,
        borderBottomColor: "#ccc",
        borderBottomWidth:1,
    },
    addButton: {
        backgroundColor: "#3D1198",
        padding: 10,
        borderRadius: 5,
        marginBottom: 10,
    },
    addButtonText: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 18,
    },
    task: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginBottom: 15,
        fontSize: 18,
    },
    itemList: {
        fontSize: 19,
        maxWidth: "65%"
    },
    taskModal: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center'
    },
    taskButtons: {
        flexDirection: "row"
    },
    editButton: {
        marginRight: 20,
        color: "#fff",
        fontWeight: "bold",
        fontSize: 18,
        backgroundColor: "#3D1198",
        padding: 10,
        borderRadius: 5,
        marginBottom: 10,
    },
    deleteButton: {
        marginRight: 10,
        color: "red",
        fontWeight: "bold",
        fontSize: 18,
    },
    closeButton: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 18,
        backgroundColor: "red",
        padding: 10,
        borderRadius: 5,
        marginBottom: 10,
    },
    Modalinput: {
        borderWidth: 3,
        borderColor: "#ccc",
        padding: 10,
        marginBottom: 10,
        borderRadius: 10,
        fontSize: 18,
        width: '60%'
    }
});
  
export default Home;