import {CREATE_TASK,CREATE_TASK_FROM_STORAGE,EDIT_TASK,DELETE_TASK} from './type'

export function create_item_from_storage(tasks_lists){
    return {
        type: CREATE_TASK_FROM_STORAGE,
        tasks_lists
    }
}
export function creation_task(value){
    return {
        type: CREATE_TASK,
        value
    }
}
export function edit_tasks(index,value){
    return {
        type:EDIT_TASK,
        index,
        value
    }
}
export function delete_tasks(index){
    return {
        type:DELETE_TASK,
        index
    }
}