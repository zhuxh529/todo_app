import React,{useEffect, useState} from 'react';
import {Text, View, ActivityIndicator, StyleSheet, NetInfo, Platform, Alert} from 'react-native';
import { Provider} from 'react-redux';
import * as SplashScreen from 'expo-splash-screen';
import { configureStore } from '@reduxjs/toolkit'
import reducers from './reducer/index';

import Home from "./home";

const _ = require('lodash');
export default App = () => {
    const [store, setStore] = React.useState(null);

    // init
    useEffect(() => {
        async function initApp() {
            SplashScreen.preventAutoHideAsync();
            await initStore();
        };
        initApp();
        
    }, []);

    // hide SplashScreen when store is set 
    useEffect(() => {
        if(!_.isNil(store)){
            SplashScreen.hideAsync();
        }
    }, [store]);


    // Init redux store
    const initStore = async() => {
        let preloadedState = {
            
        };
        let initial_store = configureStore({
            reducer: reducers,
            preloadedState
        });

        setStore(initial_store)
    };

    if(_.isNil(store)) {
        // this section is covered by SplashScreen, so it doesn't wait for it returns
        return (
            <View/>
        )
    }
    else {
        return (
            <Provider store={store}>
                <Home/>
            </Provider>
        );
    }
  }

