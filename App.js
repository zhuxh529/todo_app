import {AppRegistry} from 'react-native';
import App from './src/index';

export default App;
AppRegistry.registerComponent('todo', () => App);