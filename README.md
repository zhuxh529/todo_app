# 1. Setup Locally
##  Install packages
1. `yarn install`
2. `npx expo install`
##  Run locally
`npx expo start`

# 2. Test
`yarn run test`

# 3. Description
1. The project is built upon expo, react native, expo-local-authentication
2. State management is using redux
3. Tests are written only for reducers' unit tests